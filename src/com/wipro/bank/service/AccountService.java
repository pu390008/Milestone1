package com.wipro.bank.service;

import java.util.List;

import com.wipro.bank.bean.Account;
import com.wipro.bank.bean.Customer;
import com.wipro.bank.dao.AccountDao;

public class AccountService {

	AccountDao accountDao ;
	
	public String addAccount(Account acc) {
		for(Account account : AccountDao.getAccounts() ) {
			if(account.getAccountID()==acc.getAccountID()) {
				return "FAILURE";
			}
		}
		if(AccountDao.getAccounts().add(acc)) {
			return "SUCCESS";
		}
		return "FAILURE";
	}
	
	public List<Account> getAllAccounts(){
		return AccountDao.getAccounts();
	}
	
	public List<Customer> getAllCustomers(){
		return AccountDao.getCustomers();
	}
	
	public String transferFunds(int
			from,int to,double amount) {
		Account fromAccount = null ;
		Account toAccount = null;
		for(Account account : AccountDao.getAccounts() ) {
			if(account.getAccountID()==from) {
				fromAccount = account;
			}else if (account.getAccountID()==to) {
				toAccount = account;
			}
		}
		if(null!=fromAccount && null!= toAccount) {
			if(fromAccount.getBalance()<amount) {
				return "INSUFFICIENT FUNDS";
			}else {
				fromAccount.setBalance(fromAccount.getBalance()-amount);
				toAccount.setBalance(toAccount.getBalance()+amount);
				return "SUCCESS";
			}
		}else {
			return "sorry user doesn't exist";
		}
	}
	
	public Account getBalanceOf(int
			accountNumber) {
		for(Account account : AccountDao.getAccounts() ) {
			if(account.getAccountID()==accountNumber) {
				return  account ;
			}
		}
		return null;
	}

	
	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	
	
}
